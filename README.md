# Background

The 'Pi-Cam' project aims to aggregate my efforts towards a Raspberry Pi based webcam. The 
inspiration for the project comes from reading a blog post by David Hunt:
* http://www.davidhunt.ie/raspberry-pi-zero-with-pi-camera-as-usb-webcam
and utilizing his development work:
* https://github.com/climberhunt/uvc-gadget.git

His instructions are great for getting the webcam up and running smoothly using a Raspberry Pi Zero.

This project presumes you have followed his guidance (or worked something similar out on your own) 
and now wish to have:

* A compact 3D printed enclosure for the webcam.
* Better control over camera settings (brightness, contrast, etc.).
* A way to safely power down the Pi


<br/><br/>

----------------------------------------------------------------------------------------------------
## Project Components

The project is broken down into a few sub-directories:

1. <docs>
    * Provides all documentation on the project.

2. <enclosure>
    1. <cad-files>
        * CAD design files exported from Fusion 360.
    2. <external-references>
        * Copies of external CAD references used when designing the enclsoure.
        * Includes a README file with details on the original source for each external reference.
    3. <stl-files>
        * STL files that can be printed.
        * Prusa Slicer's *.3mf files.

3. <pi-hat>
    * Overview of wiring needed go make your own "Pi HAT"
    * Pi HAT feels like a generous term as you are wiring into the standard 40-pin Pi header but 
      there is not a formal circuit board.
    * Companion part to the <software> developed.

4. <software> 
    * Software for utilizing the pi-hat to customize the control / user experience.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Installing Customization Software on Raspberry Pi

The following presumes you already have a functional UVC camera and want to add the custom software 
included in this repository on the Raspberry Pi.

1.  Load up terminal connection to device (e.g. SSH).

2.  Clone the repository into the home directory:
    * git clone https://bitbucket.org/sudolegit/pi-cam.git

3.  Open root user's crontab for editing:
    * sudo crontab -e

4.  Append the following line:
    * @reboot /bin/bash /home/pi/pi-cam/software/startup.sh &

5.  Save file and reboot. 

Note, if you don't have UVC camera on path, consider the following example to get it setup properly 
(in path for root user):

1.  Connect to terminal (SSH to Pi)

2.  Open root session:
    * sudo su

3.  Open root profile:
    * vim /root/.profile

4.  Append the following:
    # Add uvc-gadget to path
    if [ -d /home/pi/uvc-gadget ]; then
        PATH="/home/pi/uvc-gadget:$PATH"
    fi

5.  Save file and quit (:wq)

6.  Sync!

7.  Reboot and check to see if control working as desired.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Detailed Overview

For complete details on how to use, modify, and expand this utility, please see the provided 
[Doxygen Summary](docs/Overview.html)


<br/><br/>

----------------------------------------------------------------------------------------------------
## Development History

For complete details on the what was changed for the latest release, please see
the [Changelog.md](Changelog.md)


<br/><br/>

----------------------------------------------------------------------------------------------------
## Licensing

All code is provided 'as is'. You are free to modify, distribute, etc. the code within the bounds
of the [Mozilla Public License (v2.0)](LICENSE.txt).


