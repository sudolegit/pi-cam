#!/bin/bash
####################################################################################################
# @brief            Updates the active settings (presently applied, not file) to match the values 
#                   defined in the 'parameters--default.sh' file.
# 
# @retval           0                   Script completed without errors
####################################################################################################


#===================================================================================================
# Define script constants
#---------------------------------------------------------------------------------------------------
FNAME_SCRIPT="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
FNAME_PARAMETERS_DEFAULT="parameters--default.sh"
FNAME_PARAMETERS_ACTIVE="parameters--active.sh"


#===================================================================================================
# Validate and process script parameters
#---------------------------------------------------------------------------------------------------
# Print help message and exit:
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
    
    echo -e "\nUsage:  ${FNAME_SCRIPT}\n"
    
    echo -e "Loads in the variables from the 'parameters--default.sh' script and applies them to the "
    echo -e "present camera configuration settings (but not to the 'parameters--active.sh' script).\n"
    
    echo -e "Note:  This will auto-generate the 'parameters--active.sh' script if it does not exist.\n"
    
    exit 0
fi


#===================================================================================================
# Main script execution
#---------------------------------------------------------------------------------------------------
source ${FNAME_PARAMETERS_DEFAULT}

# Apply settings to working environment.
v4l2-ctl --set-ctrl brightness=${brightness}
v4l2-ctl --set-ctrl contrast=${contrast}


