#!/bin/bash
####################################################################################################
# @brief            Pushes the active settings for the camera to the 'parameters--active.sh' file.
# 
# @param[in]        action              Defines action to take by this script:
#                                           * -h|--help => Print help information and exit
# 
# @retval           0                   Script completed without errors
####################################################################################################


#===================================================================================================
# Define script constants
#---------------------------------------------------------------------------------------------------
FNAME_SCRIPT="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
FNAME_ACTIVE_PARAMETERS="parameters--active.sh"


#===================================================================================================
# Validate and process script parameters
#---------------------------------------------------------------------------------------------------
# Print help message and exit:
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
    echo -e "\nUsage:  ${FNAME_SCRIPT}\n"
    echo -e "Saves the present configuration settings in 'parameters--active.sh' script.\n"
    exit 0
fi


#===================================================================================================
# Main script execution
#---------------------------------------------------------------------------------------------------
# Determine current settings
brightness=$(v4l2-ctl --get-ctrl brightness | cut -d ":" -f 2 | awk '{$1=$1};1')
contrast=$(v4l2-ctl --get-ctrl contrast | cut -d ":" -f 2 | awk '{$1=$1};1')

# Write settings to configuration file
echo "brightness=${brightness}" > ${FNAME_ACTIVE_PARAMETERS}
echo "contrast=${contrast}" >> ${FNAME_ACTIVE_PARAMETERS}


