# Pi Cam Software Package

When active, this set of software scripts allows changing camera state and tuning of parameters. It 
utilizes a RGB LED for user notifications, an encoder for changing values, and two buttons for 
selecting options.

Please note that if running on a Raspberry Pi Zero this will not be super responsive for rotary 
encoder changes (rotate slowly vs rapid fire).


<br/><br/>

----------------------------------------------------------------------------------------------------
## User Interface

* LED Behavior:
    * By default:
        * A 'red' color indicates the camera is inactive (video not streaming).
        * A 'green' color indicates the camera is active (video is streaming).
    * Camera color will be temporarily overridden when pressing the encoder or momentary buttons 
      as detailed in the following sections.

* Encoder:
    * Clockwise increases values (one value per tick).
    * Counterclockwise decreases values (one value per tick).

* Encoder Button:
    * Pressing and hold the button at one second increments to enter one of the following states:
        * Hold < 1.00 seconds       => State-01; LED color == off (none)
        * Hold 1.00 - 1.99 seconds  => State-02; LED color == blue
        * Hold 2.00 - 2.99 seconds  => State-03; LED color == red
        * Hold 3.00 - 3.99 seconds  => State-04; LED color == off (none)
        * Hold >= 4.00 seconds      => [No state]
    * Actions triggered if release button in a given state:
        * State-01 => (Re)Load in the saved camera configuration settings.
        * State-02 => Save present camera configuration settings.
        * State-03 => Load in the default camera configuration settings.
        * State-04 => Turn on (bring up) the WiFi network.

* Button - Mode Control:
    * Cycle through configuration modes controlled by the encoder in the following order:
        * Brightness [default state at boot]
        * Contrast
    * While the button is pressed, the LED will be updated to let you know what state you have 
      activated:
        * Brightness => LED color == blue
        * Contrast   => LED color == red

* Button - Video Output State:
    *   Toggl if video output is enabled or disabled.
    *   When video is disabled, blink the present LED color (no color change, should be green 
        if streaming without errors).
    *   Will only change state when video is streaming.

While the WiFi can always be enabled, all other encoder, encoder button, and momentary button inputs 
are ignored if not streaming (if camera inactive).

Video output can only be disabled while streaming.


<br/><br/>

----------------------------------------------------------------------------------------------------
## Overview of Files

The following files are included:

* change-brightness.sh
    * Increase or decrease the present brightness of the camera by one tick or set it to a numeric 
      value provided.
    * Will limit value changes to range supported by target device.
    * Use '--help' or '-h' for syntax.

* change-contrast.sh
    * Increase or decrease the present contrast of the camera by one tick or set it to a numeric 
      value provided.
    * Will limit value changes to range supported by target device.
    * Use '--help' or '-h' for syntax.

* check--camera-is-active.sh
    * Polls process thread for UVC to determine if camera is active.
    * Uses return code to return state (1 == on; 0 == off).

* config.py
    * Configuration file used by 'monitor_camera_state.py'.
    * Provides pin routing, polling intervals, and default values.

* dump--parameter-values.sh
    * Prints current values for file containing default parameters, file containing active 
      parameters, and the active camera settings.
    * Use for quick debugging/validation on the terminal.

* load-active-parameters.sh
    * Updates the present camera configuration settings using the values from the 
      "parameters--active.sh" file.
    * No changes are made if the "parameters--active.sh" file is not found.
    * Use '--help' or '-h' for syntax.

* load-default-parameters.sh
    * Updates the present camera configuration settings using the values from the 
      "parameters--default.sh" file.
    * Use '--help' or '-h' for syntax.

* monitor_camera_state.py
    * Main script used to monitor user input, update notification LED, and invoke relevant scripts 
      or commands to achieve the desired results.
    * Invoked by "startup.sh" at boot.
    * Must be run with "python3".

* [parameters--active.sh]
    * Contains the desired configuration settings (at boot) for the camera.
    * File is optional and is generated and overwritten whenever the "save-active-parameters.sh" 
      script is invoked.

* parameters--default.sh
    * Contains the default configuration settings for the camera.
    * Used whenever defaults are manually loaded (via "load-default-parameters.sh") or at boot if 
      there is no "paramters--active.sh" found.

* query-brightness.sh
    * Queries the system for the desired brightness setting and prints the numeric value to the 
      terminal.
    * Supports querying value, min-value required, and max-value allowed.
    * Use '--help' or '-h' for syntax.

* query-contrast.sh
    * Queries the system for the desired contrast setting and prints the numeric value to the 
      terminal.
    * Supports querying value, min-value required, and max-value allowed.
    * Use '--help' or '-h' for syntax.

* save-active-parameters.sh
    * Saves the present camera configuration settings to the "parameters--active.sh" file.
    * Use '--help' or '-h' for syntax.

* startup.sh
    * Script should be hooked to run at boot in crontab during installation.
    * Must be run as root.
    * Launches "monitor_camera_state.py" and kills network

* video-stream--disable.sh
    * Confirms there is an active UVC video thread and then kills it.

* video-stream--enable.sh
    * Confirms there is no active UVC video thread and uses the 'uvc-gadget' utility to start a new stream.
        * Requires the 'uvc-gadget' utility to be in the system path.




