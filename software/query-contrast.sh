#!/bin/bash
####################################################################################################
# @brief            Determines the current contrast value for the camera and returns the value to 
#                   the end user.
# 
# @param[in]        action              Defines action to take by this script:
#                                           * -h|--help => Print help information and exit
# 
# @retval           0                   Script completed without errors.
# @retval           1                   Invalid number of parameters provided (supports and requires 
#                                       1 parameter).
# @retval           2                   Parameter value provided is not recognized (unsupported 
#                                       value).
####################################################################################################


#===================================================================================================
# Validate and process script parameters
#---------------------------------------------------------------------------------------------------
# Require one (and only one) parameter:
if ! [ $# -eq 1 ]; then
    exit 1
fi

# Print help message and exit:
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
    FNAME_SCRIPT="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
    echo -e "\nUsage:  ${FNAME_SCRIPT} [-v|--value|-m|--min|-M|--max]\n"
    
    echo -e "  -v, --value                Return present value for contrast"
    echo -e "  -m, --min                  Return minimum value required for contrast"
    echo -e "  -M, --max                  Return maximum value allowed for contrast\n"
  
    echo -e "Query [read] the desired contrast setting for camera and print value over stdout.\n"
    
    echo -e "Uses 'v4l2-ctl' to query the current setting and then processes the returned string"
    echo -e "to extract (and then report) the numeric value.\n"
    
    exit 0
fi

# Ensure parameter value supplied is supported:
if [[ "$1" != "--value" ]] && [[ "$1" != "-v" ]] && [[ "$1" != "--min" ]] && [[ "$1" != "-m" ]] && [[ "$1" != "--max" ]] && [[ "$1" != "-M" ]]; then
    exit 2
fi


#===================================================================================================
# Main script execution
#---------------------------------------------------------------------------------------------------
# Execute relevant command and string post-processing. Result will be visible over stdout:
if [[ "$1" == "--value" ]] || [[ "$1" == "-v" ]]; then
    # Use v4l2 to query value. The value returned will be in the format "contrast: ##". We then 
    # split the string in half by ':' and trim white space from the right-half of the string. 
    v4l2-ctl --get-ctrl contrast | cut -d ":" -f 2 | awk '{$1=$1};1'
elif [[ "$1" == "--min" ]] || [[ "$1" == "-m" ]]; then
    # Use v4l2 to query value. The value returned will be in the format: 
    #   * "contrast #### (int)    : min=# max=# step=# default=# value=# flags=[string]"
    # We then split the string into segments by '=' and use the 2nd entry. The selected entry is 
    # then trimmed of excess white space.
    v4l2-ctl -l | grep "contrast" | cut -d '=' -f 2 | cut -d ' ' -f 1 | awk '{$1=$1};1'
elif [[ "$1" == "--max" ]] || [[ "$1" == "-M" ]]; then
    # Use v4l2 to query value. The value returned will be in the format: 
    #   * "contrast #### (int)    : min=# max=# step=# default=# value=# flags=[string]"
    # We then split the string into segments by '=' and use the 3rd entry. The selected entry is 
    # then trimmed of excess white space.
    v4l2-ctl -l | grep "contrast" | cut -d '=' -f 3 | cut -d ' ' -f 1 | awk '{$1=$1};1'
fi


