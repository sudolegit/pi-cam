#!/bin/bash
####################################################################################################
# @brief            Confirms a video stream is active and then kills the thread.
# 
# @retval           0                   Script completed without errors.
####################################################################################################


# A return code from pgrep == 0 indicates a thread was found (and we should then kill it).
pgrep uvc > /dev/null 2>&1
if [ $? -eq 0 ]; then
    echo "Killing active video stream"
    sudo pkill uvc
else
    echo "No active video stream found (skipping, nothing to kill)"
fi


