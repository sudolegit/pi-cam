#!/bin/bash
####################################################################################################
# @brief            Modifies the current brightness setting either by one tick (up or down) or to a
#                   specific number (action taken depends on provided parameter).
# 
# @warning          Depends on the path for the active user having 'uvc-gadget' available (update 
#                   your path in ~/.profile if it is not for your installation).
# 
# @retval           0                   Script completed without errors.
####################################################################################################


# A return code from pgrep != 0 indicates a thread was NOT found (and we should then start one).
pgrep uvc > /dev/null 2>&1
if ! [ $? -eq 0 ]; then
    echo "Starting video stream"
    # Note:  We use the '-i' to ensure the path is preserved (and thus can invoke the 'uvc-gadget' 
    # command.
    sudo -i uvc-gadget -f1 -s2 -r1  -u /dev/video1 -v /dev/video0
else
    echo "Video stream already active (skipping, nothing to start)"
fi


