#!/bin/bash
####################################################################################################
# @brief            Dumps the settings and current values for the parameters presntly controlled.
# 
# @retval           0                   Script completed without errors
####################################################################################################


#===================================================================================================
# Define script constants
#---------------------------------------------------------------------------------------------------
FNAME_PARAMETERS_DEFAULT="parameters--default.sh"
FNAME_PARAMETERS_ACTIVE="parameters--active.sh"


echo -e "\nTracked Parameter Values - Default:"
if [ -e ${FNAME_PARAMETERS_DEFAULT} ]; then
    source ${FNAME_PARAMETERS_DEFAULT}
    echo "brightness: ${brightness}"
    echo "contrast: ${contrast}"
    unset brightness
    unset contrast
else
    echo "[file not found]"
fi

echo -e "\nTracked Parameter Values - Active:"
if [ -e ${FNAME_PARAMETERS_ACTIVE} ]; then
    source ${FNAME_PARAMETERS_ACTIVE}
    echo "brightness: ${brightness}"
    echo "contrast: ${contrast}"
else
    echo "[file not found]"
fi

echo -e "\nPresent Values:"
v4l2-ctl --get-ctrl brightness
v4l2-ctl --get-ctrl contrast
echo ""


