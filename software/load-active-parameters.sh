#!/bin/bash
####################################################################################################
# @brief            Updates the active settings for the camera to match the values defined in the 
#                   'parameters--active.sh' file.
# 
# @retval           0                   Script completed without errors
# @retval           1                   No active parameters file found
####################################################################################################


#===================================================================================================
# Define script constants
#---------------------------------------------------------------------------------------------------
FNAME_SCRIPT="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
FNAME_PARAMETERS_ACTIVE="parameters--active.sh"


#===================================================================================================
# Validate and process script parameters
#---------------------------------------------------------------------------------------------------
# Print help message and exit:
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
    echo -e "\nUsage:  ${FNAME_SCRIPT}\n"
    echo -e "Loads in the variables from the 'parameters--active.sh' script and applies them to the "
    echo -e "present camera configuration settings.\n"
    exit 0
fi


#===================================================================================================
# Main script execution
#---------------------------------------------------------------------------------------------------
# Ensure there is a file to load in before proceeding
if ! [ -e ${FNAME_PARAMETERS_ACTIVE} ]; then
    exit 1
fi

# Load in settings file
source ${FNAME_PARAMETERS_ACTIVE}

# Apply value(s) to working environment
if ! [ -z ${brightness+x} ]; then
    v4l2-ctl --set-ctrl brightness=${brightness}
fi

if ! [ -z ${contrast+x} ]; then
    v4l2-ctl --set-ctrl contrast=${contrast}
fi


