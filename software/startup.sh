#!/bin/bash
####################################################################################################
# @brief            Simple startup script for launching scripts to control camera functionality and 
#                   indicators.
####################################################################################################

#===================================================================================================
# Ensure script runs within the direcotry of this installation / script (following symlinks if 
# any are found
#---------------------------------------------------------------------------------------------------
__src="${BASH_SOURCE[0]}"
while [ -h "$__src" ]; do
    # Grab next component of __src 
    _DIR_CWD="$( cd -P "$( dirname "$__src" )" && pwd )"
    __src="$(readlink "$__src")"
    
    # If $__src was a relative symlink, we need to resolve it relative to the path where the 
    # symlink file was located.
    [[ $__src != /* ]] && __src="$_DIR_CWD/$__src"
done

cd -P "$( dirname "$__src" )"


#===================================================================================================
# Define constants used in script
#---------------------------------------------------------------------------------------------------
LOG_FILE="/tmp/pi-cam.log"


#===================================================================================================
# Invoke script used to monitor the state of the camera interface
#---------------------------------------------------------------------------------------------------
python3 monitor_camera_state.py >> ${LOG_FILE} &


#===================================================================================================
# Update camera settings to match parameter tracking
#---------------------------------------------------------------------------------------------------
echo "Polling for UVC to be active ..." >> ${LOG_FILE}
while [ $(pgrep uvc | wc -l) -eq 0 ]; do 
    echo "    ... polling" >> ${LOG_FILE}
done
echo "UVC thread found [$(pgrep uvc)]" >> ${LOG_FILE}

FNAME_PARAMETERS_DEFAULT="parameters--default.sh"
FNAME_PARAMETERS_ACTIVE="parameters--active.sh"

# Load in defaults (presume exists).
source ${FNAME_PARAMETERS_DEFAULT}

# Load in active parameters (which override defaults) if file exists.
if [ -e ${FNAME_PARAMETERS_ACTIVE} ]; then
    source ${FNAME_PARAMETERS_ACTIVE}
fi

# Apply settings to camera
v4l2-ctl --set-ctrl brightness=${brightness}
v4l2-ctl --set-ctrl contrast=${contrast}


#===================================================================================================
# Ensure network off (do this at end of script to ensure it is up before taking down).
#---------------------------------------------------------------------------------------------------
echo "Polling for Network to be up ..." >> ${LOG_FILE}
max_tries=25
while [ $(sudo ifconfig wlan0 | grep 192 | wc -l) -eq 0 ]; do 
    echo "    ... polling" >> ${LOG_FILE}
    max_tries=$((max_tries - 1))
    if [ ${max_tries} -eq 0 ]; then
        break
    fi
done
echo "Killing network" >> ${LOG_FILE}
sudo ifconfig wlan0 down >> ${LOG_FILE}


