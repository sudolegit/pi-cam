####################################################################################################
# @file
# 
# @brief            Defines application-specific configuration values.
####################################################################################################


#===================================================================================================
# UVC Stream Monitoring
#---------------------------------------------------------------------------------------------------
CHANGE_STATE_BUFFER_SIZE    = 1                                                 # Size of buffer for smoothing out sudden changes in UVC stream state (history tracker).


#===================================================================================================
# Threading
#---------------------------------------------------------------------------------------------------
UPDATE_RATE_MONITOR_STREAM  = 0.100                                             # How often to poll the UVC camera stream to see if it is active.
UPDATE_RATE_LED_STATUS      = 0.50                                              # How often to update the color of the indicator LED.


#===================================================================================================
# Button settings
#---------------------------------------------------------------------------------------------------
ENCODER_BUTTON_POLLING_RATE = 0.1                                               # How often (in seconds) the encoder button is polled when active.

TUNING_MODES                = { "contrast": 1, "brightness": 2 }
DEFAULT_TUNING_MODE         = TUNING_MODES["brightness"]


####################################################################################################
# @class            Routing
# 
# @brief            Aggregates all hardware specific routing and wiring information.
####################################################################################################
class Routing():
    
    ################################################################################################
    # @class        Button1
    # 
    # @brief        Defines pin routing information for the primary button.
    # 
    # @details      Button controls tuning mode (brightness, contrast, etc.).
    ################################################################################################
    class Button1():
        pin                 = 23                                                # GPIO # for the momentary button.
    
    ################################################################################################
    # @class        Button2
    # 
    # @brief        Defines pin routing information for the secondary button.
    # 
    # @details      Button controls if video is enabled or disabled.
    ################################################################################################
    class Button2():
        pin                 = 18                                                # GPIO # for the momentary button.
    
    ################################################################################################
    # @class        Encoder
    # 
    # @brief        Defines pin routing information for the encoder.
    ################################################################################################
    class Encoder():
        pinA                = 17                                                # GPIO # for pin A on the encoder.
        pinB                = 27                                                # GPIO # for pin B on the encoder.
        button              = 22                                                # GPIO # for the button on the encoder.
    
    ################################################################################################
    # @class        LEDs
    # 
    # @brief        Defines pin routing information for the LEDs.
    ################################################################################################
    class LEDs():
        red                 = 2                                                 # GPIO # for red LED.
        green               = 3                                                 # GPIO # for green LED.
        blue                = 4                                                 # GPIO # for blue LED.


