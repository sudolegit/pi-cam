#!/bin/bash
####################################################################################################
# @brief            Modifies the current contrast setting either by one tick (up or down) or to a
#                   specific number (action taken depends on provided parameter).
# 
# @param[in]        action              Defines action to take by this script:
#                                           * -h|--help => Print help information and exit
#                                           * up        => Increase contrast
#                                           * down      => Decrease contrast
#                                           * ##        => Sets contrast to value provided
# 
# @retval           0                   Script completed without errors (contrast level changed).
# @retval           1                   Invalid number of parameters provided (supports and requires 
#                                       1 parameter).
# @retval           2                   Parameter value provided is not recognized (unsupported 
#                                       value).
# @retval           3                   Value change did not take effect (confirmation step failed).
####################################################################################################


#===================================================================================================
# Define script constants
#---------------------------------------------------------------------------------------------------
NUMERIC_VALUE_CHECK='^[0-9]+$'


#===================================================================================================
# Validate and process script parameters
#---------------------------------------------------------------------------------------------------
# Require one (and only one) parameter:
if ! [ $# -eq 1 ]; then
    exit 1
fi

# Print help message and exit:
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
    FNAME_SCRIPT="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
    echo -e "\nUsage:  ${FNAME_SCRIPT} [up|down|number]\n"
    echo -e "Change contrast setting for camera in one of three ways (based on paraemter provided):\n"
    echo -e "  1. Incrementing [up] by one tick.\n"
    echo -e "  2. Decrementing [down] by one tick.\n"
    echo -e "  3. Setting contrast level to a specified [number] value.\n"
    echo -e "Uses 'v4l2-ctl' to query current setting and to apply change. Values will be implicitly limited to the"
    echo -e "range of {0, max} (where max is system specific) by the utility.\n"
    exit 0
fi

# Ensure parameter value supplied is supported:
if [[ "$1" != "up" ]] && [[ "$1" != "down" ]] && ! [[ $1 =~ $NUMERIC_VALUE_CHECK ]]; then
    exit 2
fi


#===================================================================================================
# Main script execution
#---------------------------------------------------------------------------------------------------
# Read in current contrast setting
contrast_starting_value=$(v4l2-ctl --get-ctrl contrast | cut -d ":" -f 2 | awk '{$1=$1};1')

# Determine value to set for contrast
if [[ $1 =~ $NUMERIC_VALUE_CHECK ]]; then 
    contrast_desired=${1}
else
    if [[ "$1" == "up" ]]; then
        contrast_desired=$((contrast_starting_value + 1))
    elif [[ "$1" == "down" ]]; then
        contrast_desired=$((contrast_starting_value - 1))
    fi
fi

# Change contrast
v4l2-ctl --set-ctrl contrast=${contrast_desired} > /dev/null 2>&1

# Confirmation check that value change worked
contrast_ending_value=$(v4l2-ctl --get-ctrl contrast | cut -d ":" -f 2 | awk '{$1=$1};1')
if [ ${contrast_ending_value} -eq ${contrast_desired} ]; then
    echo "Contrast changed from '${contrast_starting_value}' to '${contrast_desired}'."
    exit 0
else
    echo "Unable to change contrast from '${contrast_starting_value}' to '${contrast_desired}' [present value = '${contrast_ending_value}']."
    exit 3
fi


