#!/bin/bash
####################################################################################################
# @brief            Modifies the current brightness setting either by one tick (up or down) or to a
#                   specific number (action taken depends on provided parameter).
# 
# @param[in]        action              Defines action to take by this script:
#                                           * -h|--help => Print help information and exit
#                                           * up        => Increase brightness
#                                           * down      => Decrease brightness
#                                           * ##        => Set brightness to value provided
# 
# @retval           0                   Script completed without errors (brightness level changed).
# @retval           1                   Invalid number of parameters provided (supports and requires 
#                                       1 parameter).
# @retval           2                   Parameter value provided is not recognized (unsupported 
#                                       value).
# @retval           3                   Value change did not take effect (confirmation step failed).
####################################################################################################


#===================================================================================================
# Define script constants
#---------------------------------------------------------------------------------------------------
NUMERIC_VALUE_CHECK='^[0-9]+$'


#===================================================================================================
# Validate and process script parameters
#---------------------------------------------------------------------------------------------------
# Require one (and only one) parameter:
if ! [ $# -eq 1 ]; then
    exit 1
fi

# Print help message and exit:
if [[ "$1" == "-h" ]] || [[ "$1" == "--help" ]]; then
    FNAME_SCRIPT="$(basename "$(test -L "$0" && readlink "$0" || echo "$0")")"
    echo -e "\nUsage:  ${FNAME_SCRIPT} [up|down|number]\n"
    echo -e "Change brightness setting for camera in one of three ways (based on paraemter provided):\n"
    echo -e "  1. Incrementing [up] by one tick.\n"
    echo -e "  2. Decrementing [down] by one tick.\n"
    echo -e "  3. Setting brightness level to a specified [number] value.\n"
    echo -e "Uses 'v4l2-ctl' to query current setting and to apply change. Values will be implicitly limited to the"
    echo -e "range of {0, max} (where max is system specific) by the utility.\n"
    exit 0
fi

# Ensure parameter value supplied is supported:
if [[ "$1" != "up" ]] && [[ "$1" != "down" ]] && ! [[ $1 =~ $NUMERIC_VALUE_CHECK ]]; then
    exit 2
fi


#===================================================================================================
# Main script execution
#---------------------------------------------------------------------------------------------------
# Read in current brightness setting
brightness_starting_value=$(v4l2-ctl --get-ctrl brightness | cut -d ":" -f 2 | awk '{$1=$1};1')

# Determine value to set for brightness
if [[ $1 =~ $NUMERIC_VALUE_CHECK ]]; then 
    brightness_desired=${1}
else
    if [[ "$1" == "up" ]]; then
        brightness_desired=$((brightness_starting_value + 1))
    elif [[ "$1" == "down" ]]; then
        brightness_desired=$((brightness_starting_value - 1))
    fi
fi

# Change brightness
v4l2-ctl --set-ctrl brightness=${brightness_desired} > /dev/null 2>&1

# Confirmation check that value change worked
brightness_ending_value=$(v4l2-ctl --get-ctrl brightness | cut -d ":" -f 2 | awk '{$1=$1};1')
if [ ${brightness_ending_value} -eq ${brightness_desired} ]; then
    echo "Brightness changed from '${brightness_starting_value}' to '${brightness_desired}'."
    exit 0
else
    echo "Unable to change brightness from '${brightness_starting_value}' to '${brightness_desired}' [present value = '${brightness_ending_value}']."
    exit 3
fi


