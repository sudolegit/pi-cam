#!/bin/bash
####################################################################################################
# @brief            Monitors if the UVC thread is up and running and actively in use. Return codes 
#                   indicate results to caller.
# 
# @retval           0                   UVC thread not found or camera is inactive.
# @retval           1                   Camera is active.
####################################################################################################

#===================================================================================================
# Define global variables/constants
#---------------------------------------------------------------------------------------------------
context_switches_log_file="/tmp/camera-status--context-switches.log"


#===================================================================================================
# Confirm UVC is up and running before formal check below
#---------------------------------------------------------------------------------------------------
pgrep uvc > /dev/null 2>&1
if ! [ $? -eq 0 ]; then
    exit 0
fi


#===================================================================================================
# Extract relevant data to check against for active state
#---------------------------------------------------------------------------------------------------
context_switches_current="$(cat /proc/$(pgrep uvc)/status | grep ^"voluntary_ctxt_switches" |  sed -e 's/\t/ /g' | cut -d ' ' -f 2)"

if [ -e ${context_switches_log_file} ]; then
    context_switches_previous="$(sed "1q;d" ${context_switches_log_file} 2>/dev/null)"
else
    context_switches_previous="${context_switches_current}"
fi

# Note:  the way the logic works, if see 'sleep' in state then UVC is streaming data.
state="$(cat /proc/$(pgrep uvc)/status | grep State | sed -e 's/\t/ /g' | cut -d ' ' -f 2)"


#===================================================================================================
# Save current state of context switches for next script iteration
#---------------------------------------------------------------------------------------------------
echo "${context_switches_current}" > ${context_switches_log_file}


#===================================================================================================
# Determine state of camera
#---------------------------------------------------------------------------------------------------
#echo "${state} == S; ${context_switches_current} != ${context_switches_previous}"
if [[ "${state}" == 'S' ]] || [[ "${context_switches_current}" != "${context_switches_previous}" ]]; then
    exit 1
else
    exit 0
fi


