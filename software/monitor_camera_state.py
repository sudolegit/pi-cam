####################################################################################################
# @brief            Provides control over tuning camera settings.
# 
# @details          Monitors the state of the UVC camera stream and user input (buttons and 
#                   encoder).
#                   
#                   @par Updates the indicator LED so that:
#                       * The color is red if there is no active stream
#                       * The color is green if there is an active stream and not changing settings
#                       * The LED momentarily turns off when the mode control button is pressed to 
#                         provide a visual confirmation to the user.
#                       * The LED toggles to indicate various state changes (saving, loading 
#                         defaults, etc.)
#                   
#                   @par Uses the encoder as:
#                       * Rotation providing input to indicate increases or decreases in values 
#                         for the current parameter being adjusted (tuned).
#                       * Very short press (<1 second) indicating load saved values.
#                       * Short press (1 second) indicating save values.
#                       * Long press (2 seconds) indicating load in default values.
#                       * Very long press (3 seconds) indicating enable network.
#                   
#                   @par Uses the mode control button as:
#                       * Button press indicating swapping between parameters to adjust (contrast, 
#                         brightness, etc.).
#                   
#                   @par Uses the video state button as:
#                       * Button press indicating toggling video on or off (virtual shutter).
####################################################################################################


#===================================================================================================
# Load in external dependencies
#---------------------------------------------------------------------------------------------------
import os
import time
import subprocess
import gpiozero
import threading
import config




#===================================================================================================
# Define constant(s)
#---------------------------------------------------------------------------------------------------
WORKING_DIRECTORY               = os.path.dirname(os.path.abspath(__file__))    # Directory containing this script.

# All possible states for encoder based on rotary state.
ENCODER_ROTARY_STATE            =   {   "inactive":             0,              # Rotary encoder is not changing value.
                                        "increasing":           1,              # Rotary encoder is increasing in value (rotating clockwise).
                                        "decreasing":           2               # Rotary encoder is decreasing in value (rotating counterclockwise).
                                    }

# All possible states for encoder button action requests.
ENCODER_BUTTON_REQUESTED_ACTION =   {   "none":                 0,              # Encoder button has no active request.
                                        "save":                 1,              # Encoder button requests that the current video tuning values be saved as the active state.
                                        "load_defaults":        2,              # Encoder button requests that the video tuning values be set to defaults.
                                        "load_saved_settings":  3,              # Encoder button requests that the video tuning values be set to the most resent saved settings.
                                        "enable_network":       4               # Encoder button requests that the WiFi network be enabled.
                                    }

# All possible states for video output.
VIDEO_OUTPUT_STATE              =   {   "normal":               0,              # Video output is normal (on or off controlled by 'videoStreamIsActive' flag.
                                        "virtual_shutter":      1               # Video output is disabled (virtual shutter).
                                    }




#===================================================================================================
# Define global variable(s)
#---------------------------------------------------------------------------------------------------
rgbLED                          = None                                          # Control over RGB LED. Instance populated in main logic loop at bottom of script (requires LED() class to be defined first). Adding here as reminder that it is a global available to all parts of script.
tracking_brightness             = { "value": 0, "min": 0, "max": 0}             # Dictionary tracking present value and the range of allowed values for brightness. Note that the main processing loop at bottom of this script will dynamically populate with proper values during startup.
tracking_contrast               = { "value": 0, "min": 0, "max": 0}             # Dictionary tracking present value and the range of allowed values for contrast. Note that the main processing loop at bottom of this script will dynamically populate with proper values during startup.
videoStreamIsActive             = 0                                             # Flag indicating the UVC video stream is active
videoOutputState                = VIDEO_OUTPUT_STATE["normal"]                  # Flag indicating the active 'VIDEO_OUTPUT_STATE{}' mode.
tuningMode                      = config.DEFAULT_TUNING_MODE                    # Present mode for tuning parameters (brightness, contrast, etc.)
encoderRotaryState              = ENCODER_ROTARY_STATE["inactive"]              # Present encoder state
encoderButtonRequestedAction    = ENCODER_BUTTON_REQUESTED_ACTION["none"]       # Present encoder button action requested




####################################################################################################
# @class
# 
# @brief            Manages RGB LED so other parts of code can simply set a desired color and 
#                   control the state of the LED as if it was any arbitrary color (e.g. toggle red 
#                   and blue LEDs).
####################################################################################################
class RGBLED(object):
    
    
    ################################################################################################
    # @brief        Main constructor for script. Initializes LED to color indicated in the provided 
    #               parameters.
    # 
    # @details      Creates instances of 'gpiozero.LED()' for controlling pins and a state tracking 
    #               flag to know what color is presently desired/active. Invokes class method 
    #               'set_color()' to set the tracking flag and update the LED to a state of "all 
    #               LEDs off".
    # 
    # @param[in]    self                Object pointer to class instance.
    # @param[in]    gpioPinRed          GPIO # for pin to use for controlling red LED.
    # @param[in]    gpioPinGreen        GPIO # for pin to use for controlling green LED.
    # @param[in]    gpioPinBlue         GPIO # for pin to use for controlling blue LED.
    # 
    # @note         If no color settings are provided, the LED will be off after initialized.
    ################################################################################################
    def __init__(self, gpioPinRed, gpioPinGreen, gpioPinBlue):
        self.rgbRed     = gpiozero.LED(gpioPinRed)
        self.rgbGreen   = gpiozero.LED(gpioPinGreen)
        self.rgbBlue    = gpiozero.LED(gpioPinBlue)
        self.color      = 0b000
        
        self.set_color(0,0,0)
    
    
    ################################################################################################
    # @brief        Updates the tracking variable for the desired output color using the provided 
    #               parameters.
    # 
    # @details      Sets the state for each of the individual red, green, blue bits in the boolean 
    #               'rgb' tracking variable so the desired color is know. Once the bits are set, 
    #               the 'self.on()' method is invoked to set the active state of the RGB LED.
    # 
    # @param[in]    self                Object pointer to class instance.
    # @param[in]    red                 Boolean flag indicating if the 'red' LED should be active.
    # @param[in]    green               Boolean flag indicating if the 'green' LED should be active.
    # @param[in]    blue                Boolean flag indicating if the 'blue' LED should be active.
    ################################################################################################
    def set_color(self, red, green, blue):
        
        if red:
            self.color |= 0b100
        else:
            self.color &= ~0b100
        
        if green:
            self.color |= 0b010
        else:
            self.color &= ~0b010
        
        if blue:
            self.color |= 0b001
        else:
            self.color &= ~0b001
        
        self.on()
    
    
    ################################################################################################
    # @brief        Updates the tracking variable for the desired output color using the provided 
    #               parameters.
    # 
    # @details      Sets the state for each of the individual red, green, blue bits in the boolean 
    #               'rgb' tracking variable so the desired color is know. Once the bits are set, 
    #               the 'self.on()' method is invoked to set the active state of the RGB LED.
    # 
    # @param[in]    self                Object pointer to class instance.
    # @retval       red,blue,green      Present color values 
    ################################################################################################
    def get_color(self):
        return self.red, self.green, self.blue
    
    
    ################################################################################################
    # @brief        Sets the active state of the RGB LED to match the color tracking variable for 
    #               the class instance.
    # 
    # @details      Uses bitwise logic to turn on any LED which has its color flag bit set to 1 and 
    #               off otherwise (in the boolean tracking variable for RGB state).
    # 
    # @note         Opting not to use 'self.off()' to first set all LEDs to off as there is a 
    #               visible flicker when going that route.
    # 
    # @param[in]    self                Object pointer to class instance.
    ################################################################################################
    def on(self):
        
        if self.color & 0b100:
            self.rgbRed.on()
        else:
            self.rgbRed.off()
        
        if self.color & 0b010:
            self.rgbGreen.on()
        else:
            self.rgbGreen.off()
        
        if self.color & 0b001:
            self.rgbBlue.on()
        else:
            self.rgbBlue.off()
    
    
    ################################################################################################
    # @brief        Ensures the LED is off.
    # 
    # @details      Sets each RGB LED member to the state of 'off'.
    # 
    # @param[in]    self                Object pointer to class instance.
    ################################################################################################
    def off(self):
        self.rgbRed.off()
        self.rgbGreen.off()
        self.rgbBlue.off()
    
    
    ################################################################################################
    # @brief        Toggles the state of the active RGB LED members.
    # 
    # @details      Uses bitwise logic to discern which LED colors are active and invokes the 
    #               'toggle()' method for each relevant color. Inactive colors (those with flag bits 
    #               set to '0' are ignored).
    # 
    # @param[in]    self                Object pointer to class instance.
    ################################################################################################
    def toggle(self):
        
        if self.color & 0b100:
            self.rgbRed.toggle()
        
        if self.color & 0b010:
            self.rgbGreen.toggle()
        
        if self.color & 0b001:
            self.rgbBlue.toggle()




####################################################################################################
# @brief            Callback function for thread used to monitor video stream status and update 
#                   the global tracking flag.
# 
# @details          Invokes the external 'check--camera-is-active.sh' script and monitors return 
#                   code vales to determine if the UVC video stream is active (return code == 1) or 
#                   inactive (return code == 0). The results are stored in the global 
#                   'videoStreamIsActive' flag.
#                   
#                   @par Once the main functionality above is complete, this function configures a
#                   timer to invoke itself again after a delay of 
#                   'config.UPDATE_RATE_MONITOR_STREAM'.
# 
# @warning          The 'check--camera-is-active.sh' script is presumed to be located within the 
#                   same working directory as the file containing this method.
####################################################################################################
def monitor_video_stream():
    
    global WORKING_DIRECTORY
    global videoStreamIsActive
    
    output = subprocess.run(["%s/check--camera-is-active.sh" % WORKING_DIRECTORY], capture_output=True)
    
    if output.returncode == 1:
        # Refresh camera settings if transitioning to camera is active state.
        if videoStreamIsActive == 0:
            pull_in_camera_settings(False)
        videoStreamIsActive = min(config.CHANGE_STATE_BUFFER_SIZE, videoStreamIsActive+1)
    else:
        videoStreamIsActive = max(0, videoStreamIsActive-1)
    
    threading.Timer(config.UPDATE_RATE_MONITOR_STREAM, monitor_video_stream).start()




####################################################################################################
# @brief            Callback function for thread used to update the indicator LED.
# 
# @details          Monitors the state of global flags and uses them to set the LED state as 
#                   follows:
#                       * Video stream inactive:        red
#                       * Video stream active:
#                           * Not tuning parameters:    green
#                           * Tuning parameters:        blue
#                   
#                   @par Once the main functionality above is complete, this function configures a
#                   timer to invoke itself again after a delay of 'config.UPDATE_RATE_LED_STATUS'.
# 
# @note             This will purposely overwrite the LED states elsewhere in this file as long as 
#                   neither button is being pressed. The result should clear temporary states set 
#                   elsewhere in the file.
####################################################################################################
def update_indicator_led():
    
    global rgbLED, videoStreamIsActive, videoOutputState, buttonEncoder, buttonModeControl
    
    threading.Timer(config.UPDATE_RATE_LED_STATUS, update_indicator_led).start()
    
    if (buttonEncoder.is_pressed and encoderButtonRequestedAction != ENCODER_BUTTON_REQUESTED_ACTION["none"]) or buttonModeControl.is_pressed:
        return
    
    # The logic below will ensure that the green led is toggled while the virtual shutter is active 
    # (in that state video stream is inactive).
    if videoOutputState == VIDEO_OUTPUT_STATE["virtual_shutter"]:
        rgbLED.toggle()
    elif videoStreamIsActive:
        rgbLED.set_color(0, 1, 0)
    else:
        rgbLED.set_color(1, 0, 0)
    




####################################################################################################
# @brief            Updates the active tracked parameter value for the UVC camera stream.
# 
# @details          Uses the global tracking parameters 'encoderRotaryState' to determine if values 
#                   should go up or down and the global 'tuningMode' to determine which value should 
#                   be updated (brightness, contrast, etc.).
#                   
#                   @par Once direction and type determined, the value is determined based on 
#                   encoder direction and using the relvant global tracking dictionaries 
#                   'tracking_...{value, min, max}'. The value is then pushed via direct calls to 
#                   the 'v4l2-ctl' utility.
# 
# @note             Earlier versions of script invoked external wrapper methods for changing values.
#                   The lag was found to be significant (on the order of seconds) so we now opt to 
#                   directly calling the 'v4l2-ctl' utility and track the values to set within this 
#                   script as global variables. For the same reason, we no longer print messages 
#                   but return values instead.
# 
# @retval           0                   Attempted to update value successfully. Note that there is 
#                                       no checks on return code or value returned due to trying to 
#                                       optimize runtime performance and the use of 'Popen' (vs say 
#                                       'run') which doesn't effectively allow us to know the status 
#                                       of the command sent.
# @retval           -1                  Cannot complete action due to unkown direction for encoder
#                                       (unsupported value for 'ENCODER_ROTARY_STATE').
# @retval           -2                  Cannot complete action due to unsupported 'tuningMode' 
#                                       value.
# @retval           >0                  Error code passed back from 'v4l2-ctl' utility.
####################################################################################################
def update_tracked_uvc_parameter_value():
    
    global ENCODER_ROTARY_STATE, TUNING_MODES
    global encoderRotaryState, tuningMode, rgbLED
    global tracking_brightness, tracking_contrast
    
    if encoderRotaryState != ENCODER_ROTARY_STATE["increasing"] and encoderRotaryState != ENCODER_ROTARY_STATE["decreasing"]:
        return -1
    
    if tuningMode == config.TUNING_MODES["brightness"]:
        
        if encoderRotaryState == ENCODER_ROTARY_STATE["increasing"]:
            tracking_brightness["value"] = min(tracking_brightness["value"]+1, tracking_brightness["max"])
        else:
            tracking_brightness["value"] = max(tracking_brightness["value"]-1, tracking_brightness["min"])
        
        output = subprocess.Popen("v4l2-ctl --set-ctrl brightness=%s" % (tracking_brightness["value"]), shell=True)
    
    elif tuningMode == config.TUNING_MODES["contrast"]:
        
        if encoderRotaryState == ENCODER_ROTARY_STATE["increasing"]:
            tracking_contrast["value"] = min(tracking_contrast["value"]+1, tracking_contrast["max"])
        else:
            tracking_contrast["value"] = max(tracking_contrast["value"]-1, tracking_contrast["min"])
        
        output = subprocess.Popen("v4l2-ctl --set-ctrl contrast=%s" % (tracking_contrast["value"]), shell=True)
    
    else:
        return -2
    
    encoderRotaryState = ENCODER_ROTARY_STATE["inactive"]
    return 0




####################################################################################################
# @brief            Callback function invoked when the state of encoder pin a is on a rising edge 
#                   transition (low-to-high).
# 
# @details          Monitors the state of 'enocerB'. If it is also high, then we know the encoder is 
#                   moving in a counterclockwise direction and the global 'encoderRotaryState' flag 
#                   is updated to reflect that the value being tuned should be decreased.
# 
# @note             Function only processes when streaming is active. If the 'videoStreamIsActive' 
#                   flag is not set, returns without taking any action.
####################################################################################################
def encoder_pin_a_rising():
    
    global ENCODER_ROTARY_STATE
    global videoStreamIsActive, encoderRotaryState
    
    if not videoStreamIsActive:
        return
    
    if encoderB.is_pressed: 
        encoderRotaryState = ENCODER_ROTARY_STATE["decreasing"]
        update_tracked_uvc_parameter_value()




####################################################################################################
# @brief            Callback function invoked when the state of encoder pin a is on a rising edge 
#                   transition (low-to-high).
# 
# @details          Monitors the state of 'enocerA'. If it is also high, then we know the encoder is 
#                   moving in a clockwise direction and the global 'encoderRotaryState' flag is 
#                   updated to reflect that the value being tuned should be increased.
# 
# @note             Function only processes when streaming is active. If the 'videoStreamIsActive' 
#                   flag is not set, returns without taking any action.
####################################################################################################
def encoder_pin_b_rising():
    
    global ENCODER_ROTARY_STATE
    global videoStreamIsActive, encoderRotaryState
    
    if not videoStreamIsActive:
        return
    
    if encoderA.is_pressed: 
        encoderRotaryState = ENCODER_ROTARY_STATE["increasing"]
        update_tracked_uvc_parameter_value()




####################################################################################################
# @brief            Callback function invoked at the designated 'hold_time' interval. Updates state 
#                   of the global 'encoderButtonRequestedAction' variable.
# 
# @details          Sets state for 'encoderButtonRequestedAction' based on duration of being held:
#                       *   0-0.99 seconds == load saved settings
#                       *   1-1.99 seconds == save present camera settings as new active parameters
#                           *   Also sets LED color to blue
#                       *   2-2.99 seconds == load default parameters
#                           *   Also sets LED color to red
#                       *   3-3.99 seconds == enable WiFi
#                           *   Also disables LED
#                       *   Other values   == no action
# 
# @note             This sets the flag. The action is handled by the 'encoder_button_when_released'
#                   function.
# 
# @note             Function only processes when streaming is active. If the 'videoStreamIsActive' 
#                   flag is not set, returns without taking any action.
# 
# @note             The LED state will be cleared by the 'update_indicator_led()' function the next 
#                   time it is invoked AND the button is no longer being pressed.
####################################################################################################
def encoder_button_when_held():
    
    global buttonModeControl, videoStreamIsActive, encoderButtonRequestedAction
    
    # Error prevention:  don't process anything if no 'pressed_time' recorded. Edge case and 
    # we've seen it pop up a few times. The issue is the 'pressed_time' variable is 'NoneType' and 
    # is compared against integer values. Such a comparisons is illegal (e.g. None >= 3) and an 
    # exception is raised.
    if type(buttonEncoder.pressed_time) is None:
        return
    
    # Always allow enabling network. All other features disabled if video not streaming.
    if buttonEncoder.pressed_time >= 3 and buttonEncoder.pressed_time < 3.9:
        rgbLED.off()
        encoderButtonRequestedAction    = ENCODER_BUTTON_REQUESTED_ACTION["enable_network"]
        return
    
    if not videoStreamIsActive:
        return
    
    if buttonEncoder.pressed_time > 3.9:
        encoderButtonRequestedAction    = ENCODER_BUTTON_REQUESTED_ACTION["none"]
    elif buttonEncoder.pressed_time >= 2:
        rgbLED.set_color(1, 0, 0)
        encoderButtonRequestedAction    = ENCODER_BUTTON_REQUESTED_ACTION["load_defaults"]
    elif buttonEncoder.pressed_time >= 1:
        rgbLED.set_color(0, 0, 1)
        encoderButtonRequestedAction    = ENCODER_BUTTON_REQUESTED_ACTION["save"]
    else:
        rgbLED.off()
        encoderButtonRequestedAction    = ENCODER_BUTTON_REQUESTED_ACTION["load_saved_settings"]




####################################################################################################
# @brief            Callback function invoked when the encoder button is released to act on the 
#                   tracked 'encoderButtonRequestedAction' variable. 
# 
# @details          Invokes external scripts based on the state of the tracking variable. Resets 
#                   the tracking variable to its inactive state afterwards.
# 
# @note             Function only processes when streaming is active. If the 'videoStreamIsActive' 
#                   flag is not set, returns without taking any action.
# 
# @returns          Nothing directly to caller but prints out actions over stdout.
####################################################################################################
def encoder_button_when_released():
    
    global videoStreamIsActive, encoderButtonRequestedAction
    
    if encoderButtonRequestedAction == ENCODER_BUTTON_REQUESTED_ACTION["enable_network"]:
        output = subprocess.run(["sudo", "ifconfig", "wlan0", "up"], capture_output=True)
        print("Enabling Network [rc = %s]" % output.returncode)
        encoderButtonRequestedAction = ENCODER_BUTTON_REQUESTED_ACTION["none"]
        return
    
    if not videoStreamIsActive:
        return
    
    if encoderButtonRequestedAction == ENCODER_BUTTON_REQUESTED_ACTION["save"]:
        output = subprocess.run(["%s/save-active-parameters.sh" % WORKING_DIRECTORY], capture_output=True)
        print("Saving camera configuraiton [rc = %s]" % output.returncode)
    elif encoderButtonRequestedAction == ENCODER_BUTTON_REQUESTED_ACTION["load_defaults"]:
        output = subprocess.run(["%s/load-default-parameters.sh" % WORKING_DIRECTORY], capture_output=True)
        pull_in_camera_settings(False)
        print("Loading Defaults [rc = %s]" % output.returncode)
    elif encoderButtonRequestedAction == ENCODER_BUTTON_REQUESTED_ACTION["load_saved_settings"]:
        output = subprocess.run(["%s/load-active-parameters.sh" % WORKING_DIRECTORY], capture_output=True)
        pull_in_camera_settings(False)
        print("Loading Saved Settings [rc = %s]" % output.returncode)
    
    encoderButtonRequestedAction = ENCODER_BUTTON_REQUESTED_ACTION["none"]




####################################################################################################
# @brief            Callback function invoked when the mode control button is pressed.
# 
# @details          Updates the state of the global 'tuningMode' flag. This flag controls what 
#                   action is taken when the encoder is rotated.
# 
# @note             Function only processes when streaming is active. If the 'videoStreamIsActive' 
#                   flag is not set, returns without taking any action.
# 
# @note             The LED state will be cleared by the 'update_indicator_led()' function the next 
#                   time it is invoked AND the button is no longer being pressed.
####################################################################################################
def mode_control_button_when_pressed():
    
    global videoStreamIsActive, tuningMode
    
    if not videoStreamIsActive:
        return
    
    if tuningMode == config.TUNING_MODES["contrast"]:
        tuningMode = config.TUNING_MODES["brightness"]
        rgbLED.set_color(0, 0, 1)
    elif tuningMode == config.TUNING_MODES["brightness"]:
        tuningMode = config.TUNING_MODES["contrast"]
        rgbLED.set_color(1, 0, 0)




####################################################################################################
# @brief            Callback function invoked when the video output state button is pressed.
# 
# @details          Updates the state of the global 'videoOutputState' flag. This flag controls what 
#                   state the output video stream should be in presently.
#                   
#                   @par In addition to updating the flag, the video state will be modified as 
#                   follows:
#                       1.  If new state is 'normal':
#                           *   Restore tracked video tuning values for brightness and contrast.
#                           *   Enable stream.
#                       2.  If new state is 'virtual_shutter':
#                           *   Set video tuning values for brightness and contrast to zero.
#                           *   Disable stream.
# 
# @note             Function only processes when streaming is active. If the 'videoStreamIsActive' 
#                   flag is not set, returns without taking any action.
# 
# @note             The LED state is not changed or managed by this function. Instead, setting the 
#                   flag changes how the active LED color is modified the next time the 
#                   'update_indicator_led()' is run (which runs automatically on a timer).
####################################################################################################
def video_output_state_button_when_pressed():
    
    global videoStreamIsActive, videoOutputState
    
    if videoOutputState == VIDEO_OUTPUT_STATE["normal"]:
        # We purposely only check if stream is active under 'normal' mode. By doing this in this
        # fashion we don't prevent re-enabling video and we ensure we don't disable video when 
        # there is presently no stream.
        if videoStreamIsActive:
            videoOutputState = VIDEO_OUTPUT_STATE["virtual_shutter"]
            # Set contrast and brightness to 0 as they will take effect faster thank killing the stream (then kill the stream).
            output = subprocess.Popen("v4l2-ctl --set-ctrl brightness=0; v4l2-ctl --set-ctrl contrast=0; %s/video-stream--disable.sh" % (WORKING_DIRECTORY), shell=True)
    elif videoOutputState == VIDEO_OUTPUT_STATE["virtual_shutter"]:
        videoOutputState = VIDEO_OUTPUT_STATE["normal"]
        # Set contrast and brightness to presently tracked values as they will longer to take effect and then enable the stream.
        output = subprocess.Popen("v4l2-ctl --set-ctrl brightness=%s; v4l2-ctl --set-ctrl contrast=%s; %s/video-stream--enable.sh" % (tracking_brightness["value"], tracking_contrast["value"], WORKING_DIRECTORY), shell=True)




####################################################################################################
# @brief            Update global 'track_*' variables that contain the information on camera 
#                   parameter settings and limits.
# 
# @details          Uses subprocess to run the necessary queries. Each parameter tracked supports 
#                   tracking active value, min value, and max value. Unfortunately, we need to 
#                   invoke an external script for each value. To reduce overhead (as min and max 
#                   only need to be determined once per launch of script), we have a 'refresh_all' 
#                   flag that if set to True will do all queries, else we will only query/update 
#                   the active values.
# 
# @param[in]        refresh_all         Flag indicating if all values should be queried [if True] or
#                                       if only the active values need to be queried for each 
#                                       'track_*' variable.
####################################################################################################
def pull_in_camera_settings(refresh_all):
    
    global tracking_brightness, tracking_contrast
    
    # Update tracking variable for brightness
    tracking_brightness["value"]      = int(subprocess.run(["%s/query-brightness.sh" % WORKING_DIRECTORY, "--value"], capture_output=True).stdout)
    if refresh_all == True:
        tracking_brightness["min"]    = int(subprocess.run(["%s/query-brightness.sh" % WORKING_DIRECTORY, "--min"], capture_output=True).stdout)
        tracking_brightness["max"]    = int(subprocess.run(["%s/query-brightness.sh" % WORKING_DIRECTORY, "--max"], capture_output=True).stdout)
    
    # Populate tracking variable for contrast
    tracking_contrast["value"]      = int(subprocess.run(["%s/query-contrast.sh" % WORKING_DIRECTORY, "--value"], capture_output=True).stdout)
    if refresh_all == True:
        tracking_contrast["min"]    = int(subprocess.run(["%s/query-contrast.sh" % WORKING_DIRECTORY, "--min"], capture_output=True).stdout)
        tracking_contrast["max"]    = int(subprocess.run(["%s/query-contrast.sh" % WORKING_DIRECTORY, "--max"], capture_output=True).stdout)




#===================================================================================================
# Main thread for script
#---------------------------------------------------------------------------------------------------
# Create instance of LED class and initialize to no color (all LEDs off)
rgbLED = RGBLED( config.Routing.LEDs.red, config.Routing.LEDs.green, config.Routing.LEDs.blue )

# Poll until UVC camera thread is up and running
while True:
    output = subprocess.run(["pgrep", "uvc"], capture_output=True)
    if output.returncode == 0:
        break

# Make sure we have the latest camera settings at start so we tune properly
pull_in_camera_settings(True)

# Define encoder pins and assign hooks
encoderA = gpiozero.Button(config.Routing.Encoder.pinA, pull_up=True)
encoderB = gpiozero.Button(config.Routing.Encoder.pinB, pull_up=True)
 
encoderA.when_pressed = encoder_pin_a_rising
encoderB.when_pressed = encoder_pin_b_rising

# Create encoder button variable. 
buttonEncoder               = gpiozero.Button(config.Routing.Encoder.button)
buttonEncoder.hold_repeat   = True
buttonEncoder.hold_time     = config.ENCODER_BUTTON_POLLING_RATE
buttonEncoder.when_held     = encoder_button_when_held
buttonEncoder.when_released = encoder_button_when_released

# Create primary mode control button variable (controls modes).
buttonModeControl               = gpiozero.Button(config.Routing.Button1.pin)
buttonModeControl.when_pressed  = mode_control_button_when_pressed

# Create video output state button variable. 
buttonVideoOutputState              = gpiozero.Button(config.Routing.Button2.pin)
buttonVideoOutputState.when_pressed = video_output_state_button_when_pressed

# Start thread for monitoring state of video stream
threading.Timer(config.UPDATE_RATE_MONITOR_STREAM, monitor_video_stream).start()

# Start thread for updating LED indicator color
threading.Timer(config.UPDATE_RATE_LED_STATUS, update_indicator_led).start()




