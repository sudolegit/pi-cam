# Overview
This directory contains copies of external CAD references used while designing the enclosure. Each 
subfolder is from a separate external source and contains a README file which contains the link from 
which the model was sourced.

# External References
-   raspberry-pi-hq-camera-2.snapshot.5
    -   CAD model of the Raspberry Pi HQ Camera PCB
-   raspberry-pi-zero-w-1.snapshot.1
    -   CAD model of the Raspberry Pi Zero W PCB


